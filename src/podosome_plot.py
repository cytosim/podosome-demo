 # generic import
import math
import os
import matplotlib.pyplot as plt
import numpy as np
import csv
import re
import pandas as pd
from scipy.optimize import curve_fit
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
#import modules
import podosome_get_and_save_results as pc
import podosome_analysis as poda
import matplotlib as mpl
mpl.rcParams['axes.linewidth'] = 1 #set the value globally

################################################################################################################################################################################
#### PLOT SETTINGS ###############################################################################################################################################################
################################################################################################################################################################################

def get_label_and_color_lists(n):
    label_name_lst = ['#%s' %(i+1) for i in range (n)]
    initial_color_name_lst = ['red','blue','orange','gray','peru','navy','pink','green','lightblue','orchid']
    lc = len(initial_color_name_lst)
    if n < lc:
        color_name_lst = initial_color_name_lst[:n % lc]
    elif n == lc:
        color_name_lst = initial_color_name_lst
    elif n > lc:
        color_name_lst = []
        i = 0
        while i < math.floor(n/lc):
            color_name_lst.extend(initial_color_name_lst)
            i = i + 1
        color_name_lst.extend(color_name_lst[:n % lc])
    return label_name_lst, color_name_lst

################################################################################################################################################################################
#### PLOT RESULTS ###############################################################################################################################################################
################################################################################################################################################################################

def plot_results(res_name = None, config_dict = None, podosome_csv = None, plt_filenames_csv = None, force_pressure_energy_csv = None, **kwargs):
    for string in res_name:
        if string == 'core_radius':
            plot_core_radius_from_orientation(results_fit_parameters = 'results/general/orientation_fit_parameters.csv', plot_filenames = plt_filenames_csv)
            plot_core_radius_comparison_all(orientation_fit_parameters = 'results/general/orientation_fit_parameters.csv', length_fit_parameters = 'results/general/length_fit_parameters.csv', filament_density_fit_parameters = 'results/general/filament_density_fit_parameters.csv', density_energy_fit_parameters = 'results/general/density_energy_fit_parameters.csv')
            plot_core_radius_comparison_average_all(orientation_fit_parameters = 'results/general/orientation_fit_parameters.csv', length_fit_parameters = 'results/general/length_fit_parameters.csv', filament_density_fit_parameters = 'results/general/filament_density_fit_parameters.csv', density_energy_fit_parameters = 'results/general/density_energy_fit_parameters.csv')
        elif string ==  'force':
            plot_forces(results_force_energy_pressure = force_pressure_energy_csv, plot_filenames = plt_filenames_csv)
            plot_forces_protrusive(results_force_energy_pressure = force_pressure_energy_csv, plot_filenames = plt_filenames_csv)
        elif string ==  'pressure':
            plot_pressure(results_force_energy_pressure = force_pressure_energy_csv, plot_filenames = plt_filenames_csv)
        elif string ==  'energy':
            plot_energy(results_force_energy_pressure = force_pressure_energy_csv, plot_filenames = plt_filenames_csv)
            plot_energy_ratio(results_force_energy_pressure = force_pressure_energy_csv)
        elif string ==  'protrusive_filaments':
            plot_protrusive_filaments(results_force_energy_pressure = force_pressure_energy_csv)
        elif string == 'length_height_core':
            dict = pc.get_dict_length_height_core(podosome_characteristics = podosome_csv, common_name = 'results/individual/res_filament_length_height')
            plot_length_as_a_function_of_height_inside_core(dict)
        elif string == 'curvature':
            results = 'results/general/'+string+'.csv'
            plot_curvature(config = config_dict, res_fname = results, podosome_characteristics = podosome_csv)
        elif string == 'compression':
            results = 'results/general/'+string+'.csv'
            plot_compression(config = config_dict, res_fname = results, podosome_characteristics = podosome_csv)
        elif string == 'filament_orientation_core':
            dict = pc.get_dict_distribution_orientation_core(podosome_characteristics = podosome_csv, common_name = 'results/individual/res_distribution_orientation_core')
            plot_filament_orientation_core(dict)
            plot_filament_orientation_core_distribution(dict)
        elif string == 'filament_orientation_outside':
            dict = pc.get_dict_distribution_orientation_outside(podosome_characteristics = podosome_csv, common_name = 'results/individual/res_distribution_orientation_outside')
            plot_filament_orientation_outside(dict)
            plot_filament_orientation_outside_distribution(dict)
        elif string == 'filament_length_core':
            dict = pc.get_dict_distribution_length_core(podosome_characteristics = podosome_csv, common_name = 'results/individual/res_distribution_length_core')
            plot_filament_length_core(dict)
            plot_filament_length_core_distribution(dict)
        elif string == 'filament_length_outside':
            dict = pc.get_dict_distribution_length_outside(podosome_characteristics = podosome_csv, common_name = 'results/individual/res_distribution_length_outside')
            plot_filament_length_outside(dict)
            plot_filament_length_outside_distribution(dict)
        else:
            results = 'results/general/'+string+'.csv'
            results_fit_parameters = 'results/general/'+string+'_fit_parameters.csv'
            plot_data_as_function_of_radius(tag = string, results = results, results_fit_parameters = results_fit_parameters, config = config_dict, podosome_characteristics = podosome_csv, plot_filenames = plt_filenames_csv)
            plot_data_as_function_of_radius_individual_podosomes(tag = string, results = results, results_fit_parameters = results_fit_parameters, config = config_dict, podosome_characteristics = podosome_csv, plot_filenames = plt_filenames_csv)

def plot_data_as_function_of_radius(config = None, results = None, results_fit_parameters = None, podosome_characteristics = None, plot_filenames = None, tag = None, **kwargs):
    ##########################
    ### Extract data #########
    ## Get x-data
    radius = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    interval = radius[1]-radius[0]
    radius_centered = [r+(interval/2) for r in radius]
    ## Get y-data
    data = pd.read_csv(results)
    data_fit = pd.read_csv(results_fit_parameters)
    data_podosome = pd.read_csv(podosome_characteristics)
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(data)) #Get list of label names and colors
	##Plot data for all podosomes
    podosome_name = data_podosome['podosome_name']
    for index in range(len(podosome_name)):
        ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c=color[index],label=label_name[index]) #Plot data points
        radius_fit = np.linspace(min(radius_centered), max(radius_centered), num=100) #Define list of x-values for plotting the fitting lines
        if tag == 'length':
            ax.plot(radius_fit,poda.fit_func_growth(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c=color[index]) #Plot fitting lines
        else:
            ax.plot(radius_fit,poda.fit_func_decay(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c=color[index]) #Plot fitting lines
	##########################
	## Plot settings #########
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    outputfilename = data_plt_filenames[tag][0]
    if tag == 'orientation':
        ax.set_ylabel('Filament orientation($^o$)',fontsize=15)
        ax.legend(fontsize=10,loc='lower left', fancybox=True, framealpha=0.8)
        ax.set_ylim([0,70])
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        outputfilename = data_plt_filenames[tag][0]
    elif tag == 'length':
        ax.legend(fontsize=11,loc='upper left', fancybox=True)
        ax.set_ylim([100,400])
        ax.set_ylabel('Filament length (nm)',fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        outputfilename = data_plt_filenames[tag][0]
    elif tag == 'filament_density':
        ax.set_ylabel('Filament density ($\mu m /\mu m^3$)',fontsize=15)
        ax.legend(fontsize=11,loc='upper right', fancybox=True, framealpha=0.3)
        ax.set_ylim([0,3000])
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        outputfilename = data_plt_filenames[tag][0]
    elif tag == 'density_energy':
        ax.set_ylabel('Density of elastic energy ($k_B\,T /\mu m^3$)',fontsize=15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax.legend(fontsize=11,loc='upper right', fancybox=True)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        ax.set_ylim([0,1.6e6])
        outputfilename = data_plt_filenames[tag][0]
    fig.savefig(outputfilename)

def plot_data_as_function_of_radius_individual_podosomes(config = None, results = None, results_fit_parameters = None, podosome_characteristics = None, plot_filenames = None, tag = None, **kwargs):
    ##########################
    ### Extract data #########
    ## Get x-data
    radius = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    interval = radius[1]-radius[0]
    radius_centered = [r+(interval/2) for r in radius]
    ## Get y-data
    data = pd.read_csv(results)
    data_fit = pd.read_csv(results_fit_parameters)
    data_podosome = pd.read_csv(podosome_characteristics)
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data #############
    label_name, color = get_label_and_color_lists(len(data)) #Get list of label names and colors
	##Plot data for all podosomes
    podosome_name = data_podosome['podosome_name']
    for index in range(len(podosome_name)):
        r_core = data_fit['radius_core'][index] #core radius value
        r_s = data_fit['parameter_c'][index] #defines the scale of the transition
        radius_transition_beginning = r_core - r_s #radial distance for the beginning of the transition
        radius_transition_ending = r_core + r_s #radial distance for the end of the transition
        plt.close()
        fig, ax = plt.subplots()
        ### General plot settings
        ax.tick_params(which='both', width=1.1)
        ax.tick_params(which='major', length=6)
        ax.tick_params(which='minor', length=4)
        ax.tick_params(axis="y", labelsize=15)
        ax.tick_params(axis="x", labelsize=15)
        ax.set_xlabel('radius (nm)',fontsize=15)
        ### Plot data and fitting lines
        radius_fit = np.linspace(min(radius_centered), max(radius_centered), num=100) #Define list of x-values for plotting the fitting lines
        if tag == 'length':
            ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="green", alpha=0.18)
            ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c="green",label=label_name[index]) #Plot data points
            ax.plot(radius_fit,poda.fit_func_growth(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c="green") #Plot fitting lines
            ax.set_ylim([100,400])
            ax.set_ylabel('Filament length (nm)',fontsize=15)
            plt.text(320,190,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
            plt.text(320,165,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
            plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        elif tag == 'orientation':
            ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="black", alpha=0.18)
            ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c="black",label=label_name[index]) #Plot data points
            ax.plot(radius_fit,poda.fit_func_decay(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c="black") #Plot fitting lines
            ax.set_ylabel('Filament orientation($^o$)',fontsize=15)
            ax.set_ylim([0,70])
            plt.text(50,8,"core",fontsize=15)
            plt.text(350,8,"outside",fontsize=15)
            plt.text(320,60,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
            plt.text(320,55,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
            plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        elif tag == 'filament_density':
            ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="orange", alpha=0.18)
            ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c="orange",label=label_name[index]) #Plot data points
            ax.plot(radius_fit,poda.fit_func_decay(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c="orange") #Plot fitting lines
            ax.set_ylabel('Filament density ($\mu m /\mu m^3$)',fontsize=15)
            ax.set_ylim([0,3000])
            plt.text(320,2500,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
            plt.text(320,2300,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
            plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        elif tag == 'density_energy':
            ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="magenta", alpha=0.18)
            ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c="magenta",label=label_name[index]) #Plot data points
            ax.plot(radius_fit,poda.fit_func_decay(radius_fit,data_fit['radius_core'][index],data_fit['parameter_a'][index],data_fit['parameter_b'][index],data_fit['parameter_c'][index]),c="magenta") #Plot fitting lines
            ax.set_ylabel('Density of elastic energy ($k_B\,T /\mu m^3$)',fontsize=15)
            plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            plt.text(320,1.4e6,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
            plt.text(320,1.26e6,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
            plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
            ax.set_ylim([0,1.6e6])
        fig.savefig('./results/plots/'+tag+str(index+1)+'.pdf')
        plt.close()

def plot_core_radius_comparison_all(orientation_fit_parameters = None, length_fit_parameters = None, filament_density_fit_parameters = None, density_energy_fit_parameters = None, **kwargs):
    data_orientation = pd.read_csv(orientation_fit_parameters)
    data_length = pd.read_csv(length_fit_parameters)
    data_filament_density = pd.read_csv(filament_density_fit_parameters)
    data_density_energy = pd.read_csv(density_energy_fit_parameters)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data_orientation)))]#Define x-axis values
    label_name, color = get_label_and_color_lists(len(data_orientation)) #Get list of label names and colors
    ax.scatter(x,data_orientation['radius_core'],linestyle='None',c='black',marker='o',s=60, label='from the orientation')
    ax.scatter(x,data_length['radius_core'],linestyle='None',c='green',marker='o',s=60, label='from the length')
    ax.scatter(x,data_density_energy['radius_core'],linestyle='None',c='magenta',marker='o',s=60, label='from the density of energy')
    ax.scatter(x,data_filament_density['radius_core'],linestyle='None',c='orange',marker='o',s=60, label='from the filament density')
    ##########################
	## Plot settings #########
    ax.legend(fontsize=10,loc='upper right', fancybox=True, framealpha=0.8)
    plt.xticks(x, label_name, rotation=35, fontsize=16)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,500])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.95)
    plt.ylabel('core radius (nm)',fontsize=20)
    fig.savefig('results/plots/core_radius_comparison.pdf')

def plot_core_radius_comparison_average_all(orientation_fit_parameters = None, length_fit_parameters = None, filament_density_fit_parameters = None, density_energy_fit_parameters = None, **kwargs):
    data_orientation = pd.read_csv(orientation_fit_parameters)
    data_length = pd.read_csv(length_fit_parameters)
    data_filament_density = pd.read_csv(filament_density_fit_parameters)
    data_density_energy = pd.read_csv(density_energy_fit_parameters)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range((5)))]#Define x-axis values
    label_name = ["orientation", "length", "density of \n energy", "filament \n density", "exp value \n from PFM"]
    ax.errorbar([x[0]],[np.average(data_orientation['radius_core'])],yerr=[np.std(data_orientation['radius_core'])] ,linestyle='None',c='black',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[1]],[np.average(data_length['radius_core'])],yerr=[np.std(data_length['radius_core'])] ,linestyle='None',c='green',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[2]],[np.average(data_density_energy['radius_core'])],yerr=[np.std(data_density_energy['radius_core'])] ,linestyle='None',c='magenta',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[3]],[np.average(data_filament_density['radius_core'])],yerr=[np.std(data_filament_density['radius_core'])] ,linestyle='None',c='orange',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[4]],[140],yerr=[22.4] ,linestyle='None',c='red',marker='^',ms = 10,elinewidth=2)
    ##########################
    ## Plot settings #########
    plt.xticks(x, label_name, rotation=45, fontsize=13)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,300])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    plt.subplots_adjust(bottom=0.22,top=0.95,left=0.15,right=0.95)
    plt.ylabel('core radius (nm)',fontsize=20)
    fig.savefig('results/plots/core_radius_comparison_average.pdf')

def plot_core_radius_from_orientation(results_fit_parameters = None, plot_filenames = None, **kwargs):
    data = pd.read_csv(results_fit_parameters) #Read fit_param_orientation_csv
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name, color = get_label_and_color_lists(len(data)) #Get list of label names and colors
    for i in range(len(x)):
        ax.scatter(x[i],data['radius_core'][i],linestyle='None',c=color[i],marker='^',s=100) #Plot data points
    ax.hlines(np.average(data['radius_core']),x[0],x[len(x)-1],colors='black',linestyle='dotted',label='average: 203 nm', lw=2) #Plot average value as an horizontal line
    ##########################
	## Plot settings #########
    plt.xticks(x, label_name, rotation=35, fontsize=16)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,300])
    ax.set_xlim([x[0]-0.5,x[len(x)-1]+0.5])
    plt.margins(0.2)
    plt.subplots_adjust(bottom=0.3,left=0.25,right=0.85)
    plt.ylabel('core radius (nm)',fontsize=20)
    output_filename = data_plt_filenames['core_radius'][0]
    fig.savefig(output_filename)

def plot_filament_orientation_core(Results):
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    i = 0
    AVG_ORIENTATION = []
    for key in Results:
        orientation_list = Results[key]['filament_orientation_core']
        x = [i+1 for value in orientation_list]
        AVG_ORIENTATION.append(np.average(orientation_list))
        ax.scatter(x,orientation_list, c=color[i], alpha = 0.05, s=20)
        ax.scatter([value+1 for value in [i]],[np.average(orientation_list)], c=color[i], label = label_name[i], s=100, marker = 's')
        i = i + 1
    x_axis = [k+1 for k in list(range(len(Results)))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0,13.5])
    ax.set_ylim([-5,95])
    plt.xticks(x_axis, label_name, rotation=40,fontsize=10)
    ax.set_ylabel('filament orientation',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(20))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(10))
    fig.savefig('results/plots/filament_orientation_core.pdf')

def plot_filament_orientation_core_distribution(Results):
    plt.close()
    fig, ax = plt.subplots()
    Orientation_all_podosomes = []
    for key in Results:
        Orientation_all_podosomes.append(Results[key]['filament_orientation_core'])
    Orientation_joined_lst = poda.get_joined_list(Orientation_all_podosomes)
    n, bins, patches = ax.hist(Orientation_joined_lst,bins=49, density=True, label='average: {:.2f}'.format(np.average(Orientation_joined_lst))+' °', color='red',ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(Orientation_joined_lst),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(Orientation_joined_lst))+' °')
    ax.set_ylabel('Probability density',fontsize=15)
    ax.set_xlabel('filament orientation',fontsize=15)
    ax.legend(fontsize=11,loc='upper left', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.98)
    ax.xaxis.set_major_locator(MultipleLocator(10))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax.set_ylim([0,4.6e-2])
    fig.savefig('results/plots/distribution_orientation_core_corrected.pdf')

def plot_filament_orientation_outside(Results):
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    i = 0
    AVG_ORIENTATION = []
    for key in Results:
        orientation_list = Results[key]['filament_orientation_outside']
        x = [i+1 for value in orientation_list]
        AVG_ORIENTATION.append(np.average(orientation_list))
        ax.scatter(x,orientation_list, c=color[i], alpha = 0.05, s=20)
        ax.scatter([value+1 for value in [i]],[np.average(orientation_list)], c=color[i], label = label_name[i], s=100, marker = 's')
        i = i + 1
    x_axis = [k+1 for k in list(range(len(Results)))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0,13.5])
    ax.set_ylim([-5,95])
    plt.xticks(x_axis, label_name, rotation=40,fontsize=10)
    ax.set_ylabel('filament orientation',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(20))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(10))
    fig.savefig('results/plots/filament_orientation_outside.pdf')

def plot_filament_orientation_outside_distribution(Results):
    plt.close()
    fig, ax = plt.subplots()
    Orientation_all_podosomes = []
    for key in Results:
        Orientation_all_podosomes.append(Results[key]['filament_orientation_outside'])
    Orientation_joined_lst = poda.get_joined_list(Orientation_all_podosomes)
    n, bins, patches = ax.hist(Orientation_joined_lst,bins=49, density=True, label='average: {:.2f}'.format(np.average(Orientation_joined_lst))+' °', color='green',ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(Orientation_joined_lst),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(Orientation_joined_lst))+' °')
    ax.set_ylabel('Probability density',fontsize=15)
    ax.set_xlabel('filament orientation',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.98)
    ax.xaxis.set_major_locator(MultipleLocator(10))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax.set_ylim([0,4.6e-2])
    fig.savefig('results/plots/distribution_orientation_outside_corrected.pdf')

def plot_filament_length_core(Results):
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    i = 0
    AVG_LENGTH = []
    for key in Results:
        length_list = Results[key]['filament_length_core']
        x = [i+1 for value in length_list]
        AVG_LENGTH.append(np.average(length_list))
        ax.scatter(x,length_list, c=color[i], alpha = 0.05, s=20)
        ax.scatter([value+1 for value in [i]],[np.average(length_list)], c=color[i], label = label_name[i], s=100, marker = 's')
        i = i + 1
    x_axis = [k+1 for k in list(range(len(Results)))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0,13.5])
    ax.set_ylim([-5,1000])
    plt.xticks(x_axis, label_name, rotation=40,fontsize=10)
    ax.set_ylabel('filament length (nm)',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(200))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(100))
    fig.savefig('results/plots/filament_length_core.pdf')

def plot_filament_length_core_distribution(Results):
    plt.close()
    fig, ax = plt.subplots()
    Length_all_podosomes = []
    for key in Results:
        Length_all_podosomes.append(Results[key]['filament_length_core'])
    Length_joined_lst = poda.get_joined_list(Length_all_podosomes)
    n, bins, patches = ax.hist(Length_joined_lst,bins=22, density=True, label='average: {:.2f}'.format(np.average(Length_joined_lst))+'nm', color='red',ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(Length_joined_lst),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(Length_joined_lst))+'nm')
    ax.set_ylabel('Probability density',fontsize=15)
    ax.set_xlabel('filament length (nm)',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.95)
    ax.xaxis.set_major_locator(MultipleLocator(200))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(50))
    ax.yaxis.set_major_locator(MultipleLocator(0.3e-2))
    ax.yaxis.set_minor_locator(MultipleLocator(0.15e-2))
    ax.set_xlim([0,1000])
    ax.set_ylim([0,1.5e-2])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    fig.savefig('results/plots/distribution_length_core_corrected.pdf')

def plot_filament_length_outside(Results):
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    i = 0
    AVG_LENGTH = []
    for key in Results:
        length_list = Results[key]['filament_length_outside']
        x = [i+1 for value in length_list]
        AVG_LENGTH.append(np.average(length_list))
        ax.scatter(x,length_list, c=color[i], alpha = 0.05, s=20)
        ax.scatter([value+1 for value in [i]],[np.average(length_list)], c=color[i], label = label_name[i], s=100, marker = 's')
        i = i + 1
    x_axis = [k+1 for k in list(range(len(Results)))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0,13.5])
    ax.set_ylim([-5,1000])
    plt.xticks(x_axis, label_name, rotation=40,fontsize=10)
    ax.set_ylabel('filament length (nm)',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(200))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(100))
    fig.savefig('results/plots/filament_length_outside.pdf')

def plot_filament_length_outside_distribution(Results):
    plt.close()
    fig, ax = plt.subplots()
    Length_all_podosomes = []
    for key in Results:
        Length_all_podosomes.append(Results[key]['filament_length_outside'])
    Length_joined_lst = poda.get_joined_list(Length_all_podosomes)
    n, bins, patches = ax.hist(Length_joined_lst,bins=50, density=True, label='average: {:.2f}'.format(np.average(Length_joined_lst))+'nm', color='green',ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(Length_joined_lst),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(Length_joined_lst))+'nm')
    ax.set_ylabel('Probability density',fontsize=15)
    ax.set_xlabel('filament length (nm)',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.95)
    ax.xaxis.set_major_locator(MultipleLocator(200))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(50))
    ax.yaxis.set_major_locator(MultipleLocator(0.3e-2))
    ax.yaxis.set_minor_locator(MultipleLocator(0.15e-2))
    ax.set_xlim([0,1000])
    ax.set_ylim([0,1.5e-2])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    fig.savefig('results/plots/distribution_length_outside_corrected.pdf')

def plot_forces_distribution(Results,common_name):
    plt.close()
    for key in Results:
        buckling_forces_list = [1e12*value for value in Results[key]['buckling_force_list_core']]
        elastic_forces_list = [1e12*value for value in Results[key]['elastic_force_list_core']]
        elastic_forces_projected_list = [1e12*value for value in Results[key]['elastic_force_projected_z_list_core']]
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(buckling_forces_list,bins='auto', density=True, label='buckling: {:.2f}'.format(np.average(buckling_forces_list))+' pN', color='steelblue', alpha = 0.9) #The histogram of the data
        n, bins, patches = ax.hist(elastic_forces_list,bins='auto', density=True, label='elastic: {:.2f}'.format(np.average(elastic_forces_list))+' pN', color='green', alpha = 0.7) #The histogram of the data
        n, bins, patches = ax.hist(elastic_forces_projected_list,bins='auto', density=True, label='elastic projected: {:.2f}'.format(np.average(elastic_forces_projected_list))+' pN', color='red', alpha = 0.7) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('Elastic forces (pN)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_length_distribution(Results,common_name):
    plt.close()
    for key in Results:
        length_list = Results[key]['length_core']
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(length_list,bins='auto', density=True, label='average: {:.2f}'.format(np.average(length_list))+' nm', color='darkorchid', alpha = 0.9) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('filament length (nm)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_length_as_a_function_of_height_inside_core(Results):
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    index = 0
    plt.close()
    fig, ax = plt.subplots()
    for key in Results:
        z = Results[key]['AVG_height']
        delta_z = z[1]-z[0]
        length_list = [value for value in Results[key]['AVG_length']]
        ax.errorbar([(value + delta_z/2) for value in Results[key]['AVG_height']], length_list, yerr=Results[key]['STD_length'], fmt='o', label = label_name[index])
        index = index + 1
        z = Results[key]['AVG_height']
    ##Plot settings
    ax.legend(fontsize=10,loc='upper right', fancybox=True, framealpha=0.8)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    ax.set_xlabel('height(nm)',fontsize=15)
    ax.set_ylabel('Filament length (nm)',fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.set_ylim([0,300])
    ax.set_xlim([0,400])
    fig.savefig('results/plots/length_height.pdf')

def plot_length_as_a_function_of_height_inside_core_normalized(Results):
    label_name, color = get_label_and_color_lists(len(Results)) #Get list of label names and colors
    index = 0
    plt.close()
    fig, ax = plt.subplots()
    for key in Results:
        length_list = [value for value in Results[key]['AVG_length']]
        percent_hight_list = [value for value in Results[key]['AVG_height_percent']]
        ax.errorbar(percent_hight_list, length_list, yerr=Results[key]['STD_length'], fmt='o', label = label_name[index])
        index = index + 1
    ##Plot settings
    ax.legend(fontsize=10,loc='upper left', fancybox=True, framealpha=0.8)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    ax.set_xlabel('height(%)',fontsize=15)
    ax.set_ylabel('Filament length (nm)',fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.set_ylim([0,300])
    ax.set_xlim([0,1])
    fig.savefig('results/plots/length_height_normalized.pdf')

def plot_orientation_distribution(Results,common_name):
    plt.close()
    for key in Results:
        orientation_list = Results[key]['orientation_core']
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(orientation_list,bins='auto', density=True, label='average: {:.2f}'.format(np.average(orientation_list))+' °', color='sienna', alpha = 0.9) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('filament orientation (degree)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_compression_distribution(Results,common_name):
    plt.close()
    for key in Results:
        compression_list = [value*100 for value in Results[key]['compression_core']]
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(compression_list,bins='auto', density=True, label='average: {:.2f}'.format(np.average(compression_list))+' %', color='goldenrod', alpha = 0.9) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('filament compression (%)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_delta_length_distribution(Results,common_name):
    plt.close()
    for key in Results:
        delta_list = Results[key]['delta_filament_core']
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(delta_list,bins='auto', density=True, label='average: {:.2f}'.format(np.average(delta_list))+' nm', color='indigo', alpha = 0.9) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('filament length variation (nm)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_energy_distribution(Results,common_name):
    plt.close()
    for key in Results:
        energy_list = [value/4e-21 for value in Results[key]['energy_core']]
        plt.clf()
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist(energy_list,bins='auto', density=True, label='average: {:.2f}'.format(np.average(energy_list))+' $k_B T$', color='darkorange', alpha = 0.9) #The histogram of the data
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.text(45,0.030,new_key.replace('_',' '), fontsize = 12)
        plt.legend(fontsize=10) #using a size in points
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('filament energy ($k_B T$)')
        ax.set_ylabel('Probability density')
        fig.savefig(common_name+new_key+'.pdf')

def plot_correlation(Results,common_name):
    plt.close()
    for key in Results:
        compression_list = [value*100 for value in Results[key]['compression_core']]
        length_list = Results[key]['length_core']
        plt.clf()
        fig, ax = plt.subplots()
        ax.scatter(length_list,compression_list)
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('Length(nm)')
        ax.set_ylabel('Compression (%)')
        fig.savefig(common_name+new_key+'.pdf')

def plot_correlation_energy(Results,common_name):
    plt.close()
    for key in Results:
        energy_list = [value/4e-21 for value in Results[key]['energy_core']]
        length_list = Results[key]['length_core']
        plt.clf()
        fig, ax = plt.subplots()
        ax.scatter(length_list,energy_list)
        ##Plot settings
        new_key = key.replace('.csv', '')
        plt.xticks(fontsize=10)
        plt.yticks(fontsize=10)
        ax.set_xlabel('Length(nm)')
        ax.set_ylabel('filament energy ($k_B T$)')
        fig.savefig(common_name+new_key+'.pdf')

def plot_curvature(config = None, res_fname = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics)
    ##########################
    ### Extract data #########
    ## Get x-data
    radius = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    interval = radius[1]-radius[0]
    radius_centered = [r+(interval/2) for r in radius]
    ## Get y-data
    data = pd.read_csv(res_fname)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(data)) #Get list of label names and colors
	##Plot data for all podosomes
    podosome_name = data_podosome['podosome_name']
    for index in range(len(podosome_name)):
        ax.scatter(radius_centered, data[podosome_name[index]], marker='o',c=color[index],label=label_name[index]) #Plot data points
        #ax.hlines(np.average(data[podosome_name[index]]),radius_centered[0],radius_centered[len(radius_centered)-1],colors=color[index], lw = 1, alpha=0.6)
	##########################
	## Plot settings #########
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    ax.set_ylabel('filament curvature ($nm^{-1}$)',fontsize=15)
    ax.set_ylim([0,0.0085])
    ax.legend(fontsize=11,loc='lower left', fancybox=True)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    ax.yaxis.set_major_locator(MultipleLocator(0.002))
    ax.yaxis.set_minor_locator(MultipleLocator(0.001))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    fig.savefig('results/plots/filament_curvature.pdf')

def plot_compression(config = None, res_fname = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics)
    ##########################
    ### Extract data #########
    ## Get x-data
    radius = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    interval = radius[1]-radius[0]
    radius_centered = [r+(interval/2) for r in radius]
    ## Get y-data
    data = pd.read_csv(res_fname)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(data)) #Get list of label names and colors
	##Plot data for all podosomes
    podosome_name = data_podosome['podosome_name']
    for index in range(len(podosome_name)):
        ax.scatter(radius_centered, [value*100 for value in data[podosome_name[index]]], marker='o',c=color[index],label=label_name[index]) #Plot data points
        #ax.hlines(np.average(data[podosome_name[index]]),radius_centered[0],radius_centered[len(radius_centered)-1],colors=color[index], lw = 1, alpha=0.6)
	##########################
	## Plot settings #########
    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    ax.set_ylabel('filament compression (%)',fontsize=15)
    ax.set_ylim([0,10])
    ax.legend(fontsize=11,loc='lower left', fancybox=True)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    ax.yaxis.set_major_locator(MultipleLocator(2))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    fig.savefig('results/plots/filament_compression.pdf')

def plot_forces(results_force_energy_pressure = None, plot_filenames = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data ############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    ax.scatter(x,data['elastic_force'],c='black',s=50, label='Elastic',marker='^')#ax.scatter(x,buckling_force_list,c='green', label='Euler buckling',marker='D')
    ax.scatter(x,data['polymerization_force'],c='orchid',s=60, label='Polymerization',marker='*')
    FORCE_VALUE_EXP = pc.__FORCE_EXP_AFM__
    ax.hlines(FORCE_VALUE_EXP[0],x[0],x[len(x)-1],colors='royalblue',linestyle='dotted',label='PFM')
    ax.fill_between(x,[FORCE_VALUE_EXP[0] - FORCE_VALUE_EXP[1] for value in x],[FORCE_VALUE_EXP[0] + FORCE_VALUE_EXP[1] for value in x], color='royalblue', alpha=0.24)
    ##########################
	## Plot settings #########
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_ylabel('Force (nN)',fontsize=15)
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.set_ylim([0,20])
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.legend(fontsize=11, fancybox=True)
    ax.yaxis.set_major_locator(MultipleLocator(5))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(2.5))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    output_filename = data_plt_filenames['force'][0]
    fig.savefig(output_filename)

def plot_forces_protrusive(results_force_energy_pressure = None, plot_filenames = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data ############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    ##########################
    ## Plot the polymerization forces only: uncomment if needed #########
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    ax.scatter(x,data['polymerization_force'],c='darkviolet',s=80,marker='*')
    ax.hlines(np.average(data['polymerization_force']),x[0],x[len(x)-1],colors='darkviolet',linestyle='dotted',label='Average value: 618 pN', lw = 3)
    ax.set_ylim([0,1.8])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.set_ylabel('$F_{polym}^{core}$(nN)',fontsize=17,color='darkviolet')
    ax.tick_params(axis='y', labelcolor='darkviolet', labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    plt.text(1,0.68,'618 pN',color = 'darkviolet',fontsize=15)
    ax.yaxis.set_major_locator(MultipleLocator(0.3))
    ax.yaxis.set_minor_locator(MultipleLocator(0.15))
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig('results/plots/protrusive_force.pdf')

def plot_pressure(results_force_energy_pressure = None, plot_filenames = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    #ax.scatter(x,data['elastic_pressure'],c='red', label='Elastic',marker='v')
    ax.scatter(x,data['elastic_pressure'],c='black',s=50, label='Elastic pressure',marker='^')
    ax.scatter(x,data['polymerization_pressure'],c='orchid',s=60, label='Polymerization pressure',marker='*')
    ax.hlines(data['pressure_exp'][0],x[0],x[len(x)-1],colors='royalblue',linestyle='dotted',label='Protrusive pressure measured by PFM')
    ##########################
    y1 = [value - data['pressure_exp_err'][0] for value in data['pressure_exp']]
    y2 = [value + data['pressure_exp_err'][0] for value in data['pressure_exp']]
    ax.fill_between(x,y1,y2, color='royalblue', alpha=0.24)
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_ylabel('Pressure (kPa)',fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.set_ylim([0,160])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.yaxis.set_major_locator(MultipleLocator(40))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(20))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    output_filename = data_plt_filenames['pressure'][0]
    fig.savefig(output_filename)

def plot_energy(plot_filenames = None, results_force_energy_pressure = None, **kwargs):
    ## Extract data from force_pressure_energy_csv
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    data_plt_filenames = pd.read_csv(plot_filenames)
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    sum_energy = []
    for index in range(len(data['elastic_energy_new'])):
        sum_energy.append(data['elastic_energy_new'][index] - data['polymerization_energy'][index])
    ax.scatter(x,sum_energy,c='forestgreen',marker='o',s=60)
    ax.scatter(x,data['elastic_energy_new'],c='black',s=60, label='$U_{elastic}^{core}$',marker='^', alpha = 0.2)
    ax.scatter(x,-data['polymerization_energy'],c='black',s=60, label='$\Delta G_{polym}$',marker='v', alpha = 0.2)
    ax.hlines(0,x[0]-1,x[len(x)-1]+1, colors='black',linestyle='dotted')
    x_new = [k for k in list(range(len(data)))]
    x_new[len(x_new)-1] = len(data) +1
    ax.fill_between(x_new,[0 for value in x],[400000 for value in x], color='red', alpha=0.2)
    ##########################
    ## Plot settings #########
    ax.legend(loc='upper right',fontsize=12, fancybox=True, framealpha=0.1)
    plt.xticks(x, label_name, rotation=40,fontsize=12)
    plt.yticks(fontsize=8)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_ylabel('$\Delta G_{polym} + U_{elastic}^{core}$ ($k_B T$)',fontsize=17)
    ax.yaxis.label.set_color('green')
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.set_ylim([-320000,320000])
    ax.yaxis.set_major_locator(MultipleLocator(100000))
    ax.yaxis.set_minor_locator(MultipleLocator(100000))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.text(2.2,150000,'thermodynamically unfavorable',color = 'red',fontsize=13)
    output_filename = data_plt_filenames['energy'][0]
    fig.savefig(output_filename)

def plot_energy_ratio(results_force_energy_pressure = None, **kwargs):
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    ax.scatter(x,data['polymerization_energy']/data['elastic_energy_new'],c='black',s=60, marker='s', alpha = 1)
    ax.hlines(np.average(data['polymerization_energy']/data['elastic_energy_new']),x[0],x[len(x)-1], colors='black',linestyle='dotted',lw=3,label='average: {:.2f}'.format(np.average(data['polymerization_energy']/data['elastic_energy_new'])) )
    x_new = [k for k in list(range(len(data)))]
    ##########################
    ## Plot settings #########
    ax.legend(loc='lower right',fontsize=12, fancybox=True, framealpha=1)
    plt.xticks(x, label_name, rotation=40,fontsize=12)
    plt.yticks(fontsize=8)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_ylabel('$-\Delta G_{polym}\, / \, U_{elastic}^{core}$',fontsize=17)
    ax.yaxis.label.set_color('black')
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.set_ylim([0,8])
    ax.yaxis.set_major_locator(MultipleLocator(2))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    fig.savefig("results/plots/energy_ratio.pdf")

def plot_protrusive_filaments(results_force_energy_pressure = None, **kwargs):
    ## Extract data from force_pressure_energy_csv
    data = pd.read_csv(results_force_energy_pressure) #Read force_pressure_energy_csv
    ##########################
    ## Plot data #############
    plt.close()
    #fig, ax = plt.subplots()
    fig, ax1 = plt.subplots()
    x = [k+1 for k in list(range(len(data)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(data))[0]
    ax1.hlines(np.average(data['AVG_orientation_membrane']),x[0],x[len(x)-1],colors='darkorange',linestyle='dotted',lw=3)
    ax1.errorbar(x,data['AVG_orientation_membrane'], yerr=data['STD_orientation_membrane'], fmt='o', c='darkorange')
    ##########################
    ## Plot settings #########
    plt.xticks(x, label_name, rotation=40,fontsize=12)
    plt.yticks(fontsize=8)
    ax1.set_ylabel('Orientation of protrusive filaments ($°$)',color='darkorange',fontsize=15)
    ax1.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax1.set_ylim([0,90])
    ax1.tick_params(axis='y', labelcolor='darkorange', labelsize=14)
    ax1.yaxis.set_major_locator(MultipleLocator(15))
    ax1.tick_params(which='both', width=1.1)
    ax1.tick_params(which='major', length=6)
    ax1.tick_params(which='minor', length=4)
    ax1.tick_params(axis="y", labelsize=14)
    ax1.tick_params(axis="x", labelsize=14)
    Number_of_protrusive_filaments_marion = [150,29,21,40,31,50,17,67,50,50]
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Number of protrusive filaments', color='darkgreen',fontsize=15)  # we already handled the x-label with ax1
    ax2.scatter(x,data['number_filament_membrane'],c='darkgreen',s=50,marker='s', alpha = 0.9)
    ax2.hlines(np.average(data['number_filament_membrane']),x[0],x[len(x)-1],colors='darkgreen',linestyle='dotted',lw=3)
    ax2.tick_params(axis='y', labelcolor='darkgreen', labelsize=14)
    ax2.set_ylim([0,140])#140 is optimal
    ax2.tick_params(which='both', width=1.1)
    ax2.tick_params(which='major', length=6)
    ax2.tick_params(which='minor', length=4)
    ax2.tick_params(axis="y", labelsize=14)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig('results/plots/protrusive_filaments.pdf')
