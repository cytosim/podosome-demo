#generic import
import os
import sys
import numpy as np
import pandas as pd
import yaml
from time import perf_counter
#import modules
sys.path.insert(0, 'src/')
import podosome_get_and_save_results as pc
import podosome_plot as pod_plt
import podosome_analysis as poda
####### MAIN
if __name__=="__main__":
    ### Start the stopwatch
    t1_start = perf_counter()
    ### Input files
    res_filenames_csv = 'results_filenames.csv' #File with results filenames
    plt_filenames_csv = 'plot_filenames.csv' #File with plot filenames
    podosome_csv = 'data_characteristics.csv' #File with the main podosome characteristics (height, core radius, ...)
    ### Config parameters
    config_yaml = open('config.yaml')
    config_dict = yaml.load(config_yaml, Loader=yaml.SafeLoader)
    ###################################################
    ##### Radial distance analysis ####################
    ###################################################
    ### Get results
    Results_analysis_radial_distance = pc.compute(lst = ['orientation'], config = config_dict, podosome_characteristics = podosome_csv)
    ### Save results
    pc.save(all_results_dict = Results_analysis_radial_distance, podosome_characteristics = podosome_csv, results_filenames = res_filenames_csv, config = config_dict)
    ### Plot results
    pod_plt.plot_results(res_name = ['orientation'], config_dict = config_dict, podosome_csv = podosome_csv, plt_filenames_csv = plt_filenames_csv)
    ###################################################
    ##### Further analysis of filament organization ###
    ###################################################
    ### Get results
    Results_analysis_filament_organization = pc.compute(lst = ['filament_orientation_core'], config = config_dict, podosome_characteristics = podosome_csv)
    ### Save results
    pc.save(all_results_dict = Results_analysis_filament_organization, podosome_characteristics = podosome_csv, results_filenames = res_filenames_csv, config = config_dict)
    ### Plot results
    pod_plt.plot_results(res_name = ['filament_orientation_core'], config_dict = config_dict, podosome_csv = podosome_csv, plt_filenames_csv = plt_filenames_csv)
    ###################################################
    ##### Force generation in the podosome core #######
    ###################################################
    #update the file podosome_csv with the core radius values computed from the fits of the filament orientation
    pc.add_core_radius_to_podosome_csv(podosome_characteristics = podosome_csv, results_fit_parameters = 'results/general/orientation_fit_parameters.csv', col_name = 'radius_core')
    ### Get results
    Results_analysis_force_core = pc.compute(lst = ['elastic_force', 'polymerization_force'], podosome_characteristics = podosome_csv)
	### Save results
    pc.save(all_results_dict = Results_analysis_force_core, podosome_characteristics = podosome_csv, results_filenames = res_filenames_csv, config = config_dict)
    #Save force, pressure and energy values into a single csv file
    pc.export_forces_pressure_energy_to_csv(podosome_characteristics = podosome_csv, results_force_energy_pressure = 'results/general/force_pressure_energy.csv', results_filenames = res_filenames_csv)
    ### Plot results
    pod_plt.plot_results(res_name = ['force', 'pressure'], config_dict = config_dict, podosome_csv = podosome_csv, plt_filenames_csv = plt_filenames_csv, force_pressure_energy_csv = 'results/general/force_pressure_energy.csv')
    ###################################################
    ### Stop the stopwatch
    t1_stop = perf_counter()
    print("Elapsed time during the whole program in seconds:",t1_stop-t1_start)
